# Introduction

The Wikipedia search results plugin for Koha will make a Wikipedia search for your query,
and display it in the OPAC search results page.

# Downloading

From the [release page](https://gitlab.com/thekesolutions/plugins/koha-plugin-wikipedia-search/-/releases) you can download the relevant *.kpz file.

# Installing

Koha's *Plugin system* allows you to install the plugin by either downloading the _*.kpz_ file or,
if configured, by searching for it in the plugin administration page.

The plugin system needs to be turned on by a system administrator.

To set up the Koha plugin system you must first make some changes to your install.

* Change `<enable_plugins>0<enable_plugins>` to `<enable_plugins>1</enable_plugins>` in your koha-conf.xml file
* Confirm that the path to `<pluginsdir>` exists, is correct, and is writable by the web server
* Restart your webserver

Once set up is complete, you are ready to go.
