package Koha::Plugin::Com::Theke::WikipediaSearch;

use Modern::Perl;

use base qw(Koha::Plugins::Base);

use C4::Auth;
use C4::Context;

use Cwd qw(abs_path);

use Mojo::JSON qw(decode_json);;
use URI::Escape qw(uri_unescape);

use File::Slurp qw(read_file);
use Cwd qw(abs_path);

our $VERSION = "{VERSION}";
our $MINIMUM_VERSION = "21.05";

## Here is our metadata, some keys are required, some are optional
our $metadata = {
    name            => 'Wikipedia Search Plugin',
    author          => 'Theke Solutions',
    date_authored   => '2022-07-09',
    date_updated    => "1900-01-01",
    minimum_version => $MINIMUM_VERSION,
    maximum_version => undef,
    version         => $VERSION,
    description     => 'This plugin performs a search on Wikipedia and displays the best result',
};

sub new {
    my ( $class, $args ) = @_;

    ## We need to add our metadata here so our base class can access it
    $args->{'metadata'} = $metadata;
    $args->{'metadata'}->{'class'} = $class;

    my $self = $class->SUPER::new($args);

    return $self;
}

sub opac_js {
    my ( $self ) = @_;

    return "<script>".read_file( abs_path( $self->mbf_path('opac.js') ) )."</script>";
}

1;
