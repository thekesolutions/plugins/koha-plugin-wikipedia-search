$('document').ready(function() {
  if(document.location.pathname === '/cgi-bin/koha/opac-search.pl') {
      let lang = ($('html').attr('lang') || 'en').split('-')[0]
      let baseUrl = 'https://'+lang+'.wikipedia.org'
      let processResult = function(result) {
          if(result.query.search.length) {
              $.ajax({
                  url: api,
                  data: { 
                      action: 'parse', 
                      format: 'json', 
                      pageid: result.query.search[0].pageid,
                      prop: 'text',
                      section: 0,
                      useskin: 'vector'
                  },
                  dataType: 'jsonp',
                  success: function(result) {
                      if(result.parse.text['*']) {
                          $(document.body).addClass('skin-minerva')
                          let response = $(result.parse.text['*'])
                          // console.log(result.parse.text)
                          let infobox = response.find('.infobox')
                          if (!infobox.length) infobox = response
                          let toggleSearch = $('<i class="toggle-wiki-search fa fa-fw fa-arrow-right"/>').css({
                            'position': 'absolute',
                            'top': '12px',
                            'right': '12px'
                          });
                          $('a', infobox).each(function() {
                            let $t = $(this)
                            let href = $t.attr('href')
                            if(href.substr(0,4) !== 'http') {
                                $t.attr('href', baseUrl+href)
                            }
                          })
                          let infoboxWrapper = $('<div></div>')
                              .css({
                                  '-ms-flex': '0 0 0%',
                                  'flex': '0 0 0%',
                                  'max-width': '0%',
                                  'position': 'relative',
                                  'overflow': 'hidden'
                              })
                              .prepend(infobox.css({
                                'border': '1px solid black',
                                'opacity': '0%'
                              }))
                              .prepend(toggleSearch)
                              .addClass('mw-parser-output order-2 order-md-2 order-lg-3')
                          $('.maincontent')
                              .after(infoboxWrapper)
                              .after(response.find('style'))
                          $('.maincontent')
                              .animate(
                                  {
                                      '-ms-flex-basis': '58.333%',
                                      'flex-basis': '58.333%',
                                      'max-width': '58.333%'
                                  }, {
                                      progress: function() {
                                          $("#floating").hcSticky('refresh')
                                      },
                                      complete: function() {
                                          $('.maincontent').removeClass('col-lg-10').addClass('col-lg-7')
                                      }
                                  }
                              )
                          infoboxWrapper
                              .animate(
                                  {
                                      '-ms-flex-basis': '25%',
                                      'flex-basis': '25%',
                                      'max-width': '25%'
                                  }, {
                                      complete: function() {
                                          infoboxWrapper.addClass('col-lg-3')
                                      }
                                  }
                              )
                          infobox
                              .animate(
                                {
                                  'opacity': '100%'
                                }
                              )
                          toggleSearch.on('click', function() {
                                if($('.maincontent').css('max-width') === '58.333%') {
                                  $({deg: 0}).animate({deg: -180}, {
                                      step: function(now) {
                                          // in the step-callback (that is fired each step of the animation),
                                          // you can use the `now` paramter which contains the current
                                          // animation-position (`0` up to `angle`)
                                          toggleSearch.css({
                                              transform: 'rotate(' + now + 'deg)'
                                          });
                                      }
                                  });
                                  $('.maincontent')
                                      .animate(
                                          {
                                              '-ms-flex-basis': '80.333%',
                                              'flex-basis': '80.333%',
                                              'max-width': '80.333%'
                                          }, {
                                              progress: function() {
                                                  $("#floating").hcSticky('refresh')
                                              },
                                              complete: function() {
                                                  $('.maincontent').removeClass('col-lg-7').addClass('col-lg-10')
                                              }
                                          }
                                      )
                                  infoboxWrapper
                                      .animate(
                                          {
                                              '-ms-flex-basis': '0%',
                                              'flex-basis': '0%',
                                              'max-width': '0%'
                                          }, {
                                              complete: function() {
                                                  infoboxWrapper.removeClass('col-lg-3').addClass('col-lg-1')
                                              }
                                          }
                                      )
                                  infobox
                                      .animate(
                                        {
                                          'opacity': '0%'
                                        }
                                      )
                                } else {
                                  $({deg: -180}).animate({deg: 0}, {
                                      step: function(now) {
                                          // in the step-callback (that is fired each step of the animation),
                                          // you can use the `now` paramter which contains the current
                                          // animation-position (`0` up to `angle`)
                                          toggleSearch.css({
                                              transform: 'rotate(' + now + 'deg)'
                                          });
                                      }
                                  });
                                  $('.maincontent')
                                      .animate(
                                          {
                                              '-ms-flex-basis': '58.333%',
                                              'flex-basis': '58.333%',
                                              'max-width': '58.333%'
                                          }, {
                                              progress: function() {
                                                  $("#floating").hcSticky('refresh')
                                              },
                                              complete: function() {
                                                  $('.maincontent').removeClass('col-lg-10').addClass('col-lg-7')
                                              }
                                          }
                                      )
                                  infoboxWrapper
                                      .animate(
                                          {
                                              '-ms-flex-basis': '25%',
                                              'flex-basis': '25%',
                                              'max-width': '25%'
                                          }, {
                                              complete: function() {
                                                  infoboxWrapper.removeClass('col-lg-1').addClass('col-lg-3')
                                              }
                                          }
                                      )
                                  infobox
                                      .animate(
                                        {
                                          'opacity': '100%'
                                        }
                                      )
                                }
                              })
                      }
                  }
              });
          }
      }
      let q = document.location.search
          .replace('?', '')
          .split('&')
          .map(function(param) {
              return param.split('=')
          })
          .find(function(param) {
              return param[0] === 'q'
          })[1];
      let api = baseUrl+'/w/api.php'
      $.ajax({
          url: api,
          data: { 
              action: 'query', 
              list: 'search', 
              srsearch: q, 
              format: 'json', 
              srlimit: 1 
          },
          dataType: 'jsonp',
          success: processResult
      });
  }
})